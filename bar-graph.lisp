(in-package :vecto-graphs)

(defvar *bar-width* 10)
(defvar *bar-space* 4)

(defun draw-bars (points &key stacked)
  (loop for group in points
        do
        (loop with y-start = 0.5
              for point in group
              for color in *colors*
              do
              (apply #'set-stroke-color color)
              (apply #'set-fill-color color)
              (draw-rectangle (point (floor (- (x point)
                                               (floor *bar-width* 2)))
                                     y-start)
                              *bar-width*
                              (ceiling (y point)))
              (when stacked
                (incf y-start (ceiling (y point)))))))

(defun scale-bars (alist &key width height max-y stacked)
  (let ((x-step (floor width (1+ (length alist)))))
    (loop for center from x-step by x-step
          for (nil . values) in alist
          collect
          (if stacked
              (loop for value in values
                    collect (point center
                                   (if (zerop max-y)
                                       value
                                       (/ (* value height) max-y))))
              (loop with number = (length values)
                    with step = (+ *bar-width* *bar-space*)
                    for x from (- center
                                  (* (/ step 2) (1- number)))
                    by step
                    for value in values
                    collect (point x
                                   (if (zerop max-y)
                                       value
                                       (/ (* value height) max-y))))))))

(defun draw-bar-graph (alist &key x-label y-label
                                  arrows
                                  stacked
                                  max-y
                                  percentage)
  (let ((max-value (or (max-value alist :stacked stacked)
                       max-y)))
    (multiple-value-bind (origin x-length y-length)
        (draw-number-axes (mapcar #'car alist)
                          max-value
                          10
                          x-label y-label
                          :arrows arrows
                          :percentage percentage)
      (translate origin)
      (draw-bars (scale-bars alist
                             :width x-length :height y-length
                             :max-y max-value
                             :stacked stacked)
                 :stacked stacked))))

(defun bar-graph (file alist &key x-label
                                  y-label
                                  max-y
                                  arrows
                                  stacked)
  "Alist (name value color)"
  (with-graph (file)
    (draw-bar-graph alist
                    :x-label x-label
                    :y-label y-label
                    :arrows arrows
                    :stacked stacked
                    :max-y max-y)))
