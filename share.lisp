(in-package :vecto-graphs)

(defvar *backend*)

(defun parse-color (string)
  (loop for i from 2 by 2 to (length string)
        collect (/ (parse-integer string :start (- i 2) :end i
                                         :radix 16)
                   255.0)))
(defvar *colors*
  (mapcar #'parse-color '("7cb5ec"
                          "434348"
                          "90ed7d"
                          "f7a35c"
                          "8085e9"
                          "f15c80"
                          "e4d354"
                          "2b908f"
                          "f45b5b"
                          "91e8e1")))

(defvar *font* (zpb-ttf:open-font-loader
                (merge-pathnames "DejaVuSans.ttf"
                                 #.(or *compile-file-truename*
                                       *load-truename*))))
(defvar *font-size* 12)
(defvar *axis-font-size* 10)
(defvar *width* 500)
(defvar *height* 500)
(defvar *margins* 5)
(defvar *leading-ratio* 8/7)

(defvar *current-font* nil)
(defvar *current-font-size* nil)

(defun font ()
  (values *current-font*
          *current-font-size*))

(defun font-size ()
  *current-font-size*)

(defun (setf font) (font &optional (size *current-font-size*))
  (set-font font size)
  (setf *current-font* font
        *current-font-size* size))

(defun (setf font-size) (size)
  (setf (font size) *current-font*))

(defun total-count (alist)
  (reduce #'+ alist :key #'cdr))

(defun max-value (alist &key stacked)
  (reduce #'max alist :key (if stacked
                               (lambda (x) (reduce #'+ (cdr x)))
                               (lambda (x) (reduce #'max (cdr x) :initial-value 0)))
                      :initial-value 0))

(defun min-value (alist)
  (reduce #'min alist :key #'second))

(defun backend-from-file (file)
  (cdr (assoc (pathname-type file) '(("png" . :vecto)
                                     ("pdf" . :pdf))
              :test #'equalp)))

;;;
(defmacro def-backend-fun (name ll)
  (let ((gf-name (intern (format nil "~a~a" 'b- name))))
   `(progn
      (defgeneric ,gf-name (backend ,@ll))
      (defun ,name ,ll
        (,gf-name *backend* ,@ll)))))
(defgeneric call-with-graph (backend file width height function))
(def-backend-fun set-stroke-color (r g b))
(def-backend-fun set-fill-color (r g b))
(def-backend-fun draw-line* (from-x from-y to-x to-y))
(def-backend-fun draw-rectangle (origin width height))
(def-backend-fun set-font (font size))
(def-backend-fun %draw-string (x y string))
(def-backend-fun string-box (string))
(def-backend-fun ymax (bbox))
(def-backend-fun xmax (bbox))
(def-backend-fun ymin (bbox))
(def-backend-fun xmin (bbox))
(def-backend-fun translate (point))
(def-backend-fun rotate (angle))
(def-backend-fun stroke ())
(def-backend-fun arc (center radius start-angle end-angle))
(def-backend-fun move-to (point))
(def-backend-fun line-to (point))

;;;

(defmacro with-graph ((file &key width height) &body body)
  (let ((file-sym (gensym "FILE")))
    `(let* ((*width* (or ,width *width*))
            (*height* (or ,height *height*))
            (,file-sym ,file)
            (*backend* (backend-from-file ,file-sym))
            (*current-font-size* *font-size*))
       (call-with-graph *backend*
                        ,file-sym
                        *width*
                        *height*
                        (lambda () ,@body)))))

(defmacro with-graph-context ((type &key width height) &body body)
  `(let* ((*backend* ,type)
          (*width* (or ,width *width*))
          (*height* (or ,height *height*))
          (*current-font-size* *font-size*))
     ,@body))

(defun thin-line (x)
  (- (ceiling x) .5))

(defun bbox-width (box)
  (- (xmax box)
     (xmin box)))

(defun bbox-height (box)
  (- (ymax box)
     (ymin box)))

(defun string-width (string)
  (if (consp string)
      (reduce #'+ string :key #'string-width)
      (bbox-width (string-box string))))

(defun string-height (string)
  (if (consp string)
      (reduce #'max string :key #'string-height)
      (bbox-height (string-box string))))

(defun format-number (number &optional percent)
  (format nil "~,1f~:[~;%~]"
          number
          percent))

(defstruct rectangle
  left
  right
  )
