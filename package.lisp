(defpackage :vecto-graphs
  (:use :cl)
  (:export
   #:pie-chart
   #:bar-graph
   #:line-chart
   #:*width*
   #:*height*
   #:*bar-width*
   #:draw-bar-graph
   #:with-graph
   #:with-graph-context
   #:*margins*
   #:draw-pie-chart
   #:pie-chart-width))
