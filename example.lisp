(in-package :vecto-graphs)

(defun test-bar-graph (&optional (file "/tmp/bar-graph.pdf"))
  (vecto-graphs:bar-graph file
                          '(("African" 40)
                            ("European" 36)
                            ("European" 36))
                          :x-label "Airspeed velocity of an unladen swallow"
                          :y-label "km/h"))


(defun test-bar-graph-4 (&optional (file "/tmp/bar-graph.pdf"))
  (vecto-graphs:bar-graph file
                          '(("AAAAAAAAAAAAAAAAAAAAAA" 432704739/50 154001  154000  154000 )
                            ("BBBBBBBBBBBBBBBBBBBBBB" 1046126 998434  154000  154000)
                            ("Caaaaaaaa aaaa" 5583955 1570458  154000  154000)
                            ("Bursaries" 4753369/5 140000  154000  154000))
                          :title "Training"
                          :x-label '("Budget" "Spend")
                          :y-label "aa"
                          :stacked t))

(defun test-bar-graph-stacked (&optional (file "/tmp/bar-graph.pdf"))
  (vecto-graphs:bar-graph file
                          '(("Numeracy & Literacy (ABET)" 10 40 40 10)
                            ("Internships" 20 30 15 35)
                            ("Learnerships" 30 20 15 35)
                            ("Bursaries" 20 30 35 15))
                          :title "Training"
                          :max-y 100
                          :x-label '("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAa" "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB" "CCCCCCCCCCCCCCCCCCCCCCCCCCc" "DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD")
                          :y-label "aa"
                          :stacked t))



(defun test-line-chart (&optional (file "/tmp/line-chart.png"))
  (vecto-graphs:line-chart file
                           '(("Debt" (2006 8.4) (2007 8.9) (2008 10) (2009 11.8) (2010 12.3))
                             ("GDP" (2006 13.3) (2007  13.9) (2008 14.3) (2009 14) (2010 14.5)))
                           :x-label "US debt vs GDP"
                           :y-label "Trillion $"))

(defun test-pie (&optional (file "/tmp/pie.pdf"))
  (vecto-graphs:pie-chart file
                          '(
                            ("L" . 50)
                            
                            ("A" . 0.1)
                            ("B" . 0.1)
                            ("C" . 0.2)
                            ("D" . 0.2)
                            ("M" . 40))
                          :circle-radius 100))
