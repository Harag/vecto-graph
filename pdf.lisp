(in-package :vecto-graphs)

(defmethod call-with-graph ((backend (eql :pdf)) file width height function)
  (dx-pdf:with-pdf (file)
    (dx-pdf::new-page :orientation :landscape)
    (funcall function)))

;;;

(defmethod b-translate ((backend (eql :pdf)) point)
  (dx-pdf::transform-matrix (vector 1 0 0 1 (x point) (y point))))

(defmethod b-set-stroke-color ((backend (eql :pdf)) r g b)
  (setf (dx-pdf:stroke-color)
        (logior
         (ash (truncate (* r 255)) 16)
         (ash (truncate (* g 255)) 8)
         (truncate (* b 255)))))

(defmethod b-set-fill-color ((backend (eql :pdf)) r g b)
  (setf (dx-pdf:fill-color)
        (logior
         (ash (truncate (* r 255)) 16)
         (ash (truncate (* g 255)) 8)
         (truncate (* b 255)))))

(defmethod b-draw-line* ((backend (eql :pdf)) from-x from-y to-x to-y)
  (dx-pdf::write-object
   (dx-pdf::make-line :start-x from-x :start-y from-y
                      :end-x to-x :end-y to-y)))

(defmethod b-set-font ((backend (eql :pdf)) font size)
  )


(defmethod b-string-box ((backend (eql :pdf)) string)
  (dx-pdf::string-bounding-box (ensure-string string) (font-size)))

(defmethod b-ymax ((backend (eql :pdf)) bbox)
  (dx-pdf::string-bbox-height bbox))

(defmethod b-xmax ((backend (eql :pdf)) bbox)
  (dx-pdf::string-bbox-width bbox))

(defmethod b-ymin ((backend (eql :pdf)) bbox)
  0)

(defmethod b-xmin ((backend (eql :pdf)) bbox)
  0)

(defmethod b-%draw-string ((backend (eql :pdf)) x y string)
  (dx-pdf::write-object (dx-pdf::make-text string
                                           :x x
                                           :y y
                                           :size (font-size))))

(defmethod b-draw-rectangle ((backend (eql :pdf)) origin width height)
  (dx-pdf::write-object
   (dx-pdf::make-rectangle :x (x origin)
                           :y (y origin)
                           :width width :height height
                           :fill t)))

(defmethod b-rotate ((backend (eql :pdf)) angle)
  (let ((cos (cos angle))
        (sin (sin angle)))
    (dx-pdf::transform-matrix (vector cos sin (- sin) cos 0 0))))

;;; Taken from vecto
(defun draw-arc-curves (curves)
  (destructuring-bind (((startx . starty) &rest ignored-curve)
                       &rest ignored-curves)
      curves
    (declare (ignore ignored-curve ignored-curves))
    (dx-pdf::line-to startx starty))
  (loop for ((x1 . y1)
             (cx1 . cy1)
             (cx2 . cy2)
             (x2 . y2)) in curves
        do (dx-pdf::curve-to cx1 cy1 cx2 cy2 x2 y2)))

(defmethod b-arc ((backend (eql :pdf)) center radius start-angle end-angle)
  (loop while (< end-angle start-angle) do (incf end-angle (* 2 pi)))
  (let ((curves
          (vecto::approximate-elliptical-arc (x center) (y center) radius radius 0
                                             start-angle end-angle)))
    (draw-arc-curves curves))
  (dx-pdf:fill-path))

(defmethod b-move-to ((backend (eql :pdf)) point)
  (dx-pdf::move-to (x point) (y point)))

(defmethod b-line-to ((backend (eql :pdf)) point)
  (dx-pdf::line-to (x point) (y point)))

(defmethod b-stroke ((backend (eql :pdf)))
  (dx-pdf:stroke))
