(in-package :vecto-graphs)

(defvar *previous-pie-labels*)

(defun sector-label-dimensions (center radius start-angle angle )
  (let* ((half-angle (+ start-angle (/ angle 2.0)))
         (x (cos half-angle))
         (y (sin half-angle))
         (quadrant (quadrant half-angle))
         (align-x (ecase quadrant
                    ((1 4) :left)
                    ((2 3) :right)))
         (align-y (ecase quadrant
                    ((2 1) :bottom)
                    ((3 4) :top)))
         (start (add center
                     (* x (+ radius 5)) (* y (+ radius 5))))
         (leading (* *axis-font-size* *leading-ratio*)))
    (loop for (prev-align . prev) in (reverse *previous-pie-labels*)
          for diff = (- (y prev) (y start))
          do
          (cond ((or (not (eq prev-align align-x))
                     (> (abs diff) leading)))
                ((eq align-y :bottom)
                 (incf (y start) (+ leading diff)))
                (t
                 (decf (y start) (- leading diff)))))
    (push (cons align-x start) *previous-pie-labels*)
    (values start x y align-x align-y)))

(defun draw-sector-labels (name value
                           center radius
                           start-angle angle
                           percent)
  (multiple-value-bind (start x y align-x align-y)
      (sector-label-dimensions center radius start-angle angle )
    (draw-line (add center (* x radius) (* y radius)) start)
    (setf (font-size) *axis-font-size*)
    (set-fill-color 0 0 0)
    (draw-string start
                 (format nil "~a ~a" name (format-number value percent))
                 :align-x align-x
                 :align-y align-y)))

(defun transform-pie-data (data total)
  (loop for (name . value) in data
        unless (zerop value)
        collect (cons name (* (/ value total) 100))))

(defun draw-pie-chart (alist center radius &key percent)
  (let* ((total (total-count alist))
	 (alist (transform-pie-data alist total))
	 (sorted alist)
         *previous-pie-labels*)
    (loop for color in *colors*
          for (name . value) in sorted
          for start-angle = (+ (* 2 pi) (/ pi 2)) then end-angle
          for end-angle = (- start-angle (/ (* value pi 2) 100))
          do
          (move-to center)
          (apply #'set-fill-color color)
          (apply #'set-stroke-color color)
          (arc center radius end-angle start-angle)
          (draw-sector-labels name value
                              center radius
                              end-angle (- start-angle end-angle)
                              percent))))

(defun pie-chart-width (alist center radius &key percent)
  (let* ((total (total-count alist))
	 (alist (transform-pie-data alist total))
	 (sorted alist)
         *previous-pie-labels*
         (max-x (+ (x center) radius))
         (min-x (- (x center) radius)))
    (loop for color in *colors*
          for (name . value) in sorted
          for label = (format nil "~a ~a" name (format-number value percent))
          for start-angle = (+ (* 2 pi) (/ pi 2)) then end-angle
          for end-angle = (- start-angle (/ (* value pi 2) 100))
          do
          (multiple-value-bind (start x y align-x align-y)
              (sector-label-dimensions 
               center radius end-angle (- start-angle end-angle))
            (declare (ignore x y))
            (multiple-value-bind (origin bbox)
                (string-dimensions start label :align-x align-x
                                               :align-y align-y)
              (ecase align-x
                (:left
                 (setf max-x
                       (max max-x (+ (x origin) (bbox-width bbox)))))
                (:right
                 (setf min-x
                       (min (x origin))))))))
    (- max-x min-x)))

(defun pie-chart (file alist &key circle-radius)
  "Alist (name value)"
  (with-graph (file)
    (draw-pie-chart alist (point 250 250) circle-radius)))
